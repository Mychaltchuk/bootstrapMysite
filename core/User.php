<?php
class User {
    public $login;
    public $password;
    public $username;
    public $userfamily;
    public $userp;
    public $birthday;
    public $email;
    public $mobile;
    public $facebook;
    
    public function registerNewUser()
    {
        $sql = "INSERT INTO users("
                . "username, "
                . "userfamily, "
                . "userp, "
                . "login, "
                . "password, "
                . "email, "
                . "mobile, "
                . "facebook, "
                . "birthday, "
                . "ip) "
                . "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute([
            $this->username,
            $this->userfamily,
            $this->userp,
            $this->login,
            $this->password,
            $this->email,
            $this->mobile,
            $this->facebook,
            $this->birthday,
            $this->ip
            ]);
        $goot['goot'] = "Ви успішно зареєструвались. Дякуємо що Ви з нами";
        return $goot;
    }
    public function authUser()
    {   
        $stmt = DataBase::handler()->prepare("SELECT * FROM users WHERE login=:login AND password=:password");
        $stmt->bindParam(':login', $this->login);
        $stmt->bindParam(':password', $this->password);
        $stmt->execute();
        $res = $stmt->fetchAll();
        if(empty($res)){
            return 'Ви ввели невірно логін або пароль';
        }else{
            
            $_SESSION['online'] = true;
            $_SESSION['user_id'] =$res[0]['user_id'];
            $_SESSION['username'] =$res[0]['username'];
            $_SESSION['userfamily'] =$res[0]['userfamily'];
            $_SESSION['userp'] =$res[0]['userp'];
            $_SESSION['login'] =$res[0]['login'];
            $_SESSION['password'] =$res[0]['password'];
            $_SESSION['email'] =$res[0]['email'];
            $_SESSION['mobile'] =$res[0]['mobile'];
            $_SESSION['facebook'] =$res[0]['facebook'];
            $_SESSION['birthday'] =$res[0]['birthday'];
        }
    }
    public function userView(){
        $st = DataBase::handler()->query("SELECT * FROM users");
        return $st->fetchAll();
    }
}
