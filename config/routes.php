<?php
return array(
    ""                              => "home/index",
    "index"                         => "home/index",
    "category/([0-9]+)"             => "category/view/$1",
    "users/regestration"            => "users/formRegestration",
    "users/register"                => "users/register",
    "users/logout"                  => "users/logout",
    "users/view"                    => "users/view",
    "users/authorization"           => "users/authorization",
    "category/edit"                 => "category/edit",
    "article/edit"                  => "article/edit",
    "article/([0-9]+)"              => "article/articleById/$1",
);
