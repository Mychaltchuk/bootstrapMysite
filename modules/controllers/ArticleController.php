<?php
class ArticleController extends Controller
{
    public function articleByIdAction($param = [])
   {    
        $article_id = $param[0];
        $article['categories'] = CategoryModel::getCategories();
        $article['onearticle'] = ArticleModel::getArticleById($article_id);
        $content = View::getContents(ROOT . "/modules/views/article/view.tpl", $article);
        $this->mainView->addParam("content", $content);
        $this->mainView->display();
   }
}
