<?php
class UsersController extends Controller 
{
    public function formRegestrationAction()
    {
        if(!isset($_POST['addUser'])){
        $register['categories'] = CategoryModel::getCategories();
        $content = View::getContents(ROOT . "/modules/views/users/usersregestration.tpl", $register);
        $this->mainView->addParam("content", $content);
        $this->mainView->display();
        } else {
            $register['categories'] = CategoryModel::getCategories();
            $register['resultstate'] = UsersModel::register();
            $content = View::getContents(ROOT . "/modules/views/users/usersregestration.tpl", $register);
            $this->mainView->addParam("content", $content);
            $this->mainView->display();
    }
    }
    public function authorizationAction()
    {   
        if(!isset($_POST['authUser'])){
        $auth['categories'] = CategoryModel::getCategories();
        $content = View::getContents(ROOT . "/modules/views/users/authorization.tpl", $auth);
        $this->mainView->addParam("content", $content);
        $this->mainView->display();
        } else {
            $auth['categories'] = CategoryModel::getCategories();
            $auth['resultstate'] = UsersModel::authorization();
            $content = View::getContents(ROOT . "/modules/views/users/authorization.tpl", $auth);
            $this->mainView->addParam("content", $content);
            $this->mainView->display();
        }
    }
    public function logoutAction(){
        session_unset();
        $home['categories'] = CategoryModel::getCategories();
        $home['articles'] = ArticleModel::getArticles();
        $content = View::getContents(ROOT . "/modules/views/home/home.tpl", $home);
        $this->mainView->addParam("content", $content);
        $this->mainView->display();
    }
    public function viewAction(){
        if($_SESSION['login']=='admin'){
            $view['viewusers'] = UsersModel::viewUsers();
            $content = View::getContents(ROOT . "/modules/views/users/viewAdmin.tpl", $view);
            $this->mainView->addParam('content', $content);
            $this->mainView->display();
        } else {
            return "У Вас немає прав доступу";
        }
    }
}