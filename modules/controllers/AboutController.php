<?php

class AboutController extends FrontController {
      public function MeAction(){
        $content = View::GetContents(ROOT."/modules/views/about/autobiography.tpl");
        self::$mainView->addParam("content", $content);
    
      }
}
