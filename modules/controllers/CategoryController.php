<?php
class CategoryController extends Controller
{
    public function viewAction($param = [])
    {
        $category_id = $param[0];
        $categories = CategoryModel::getCategories();
        $articles = ArticleModel::getArticlesByCategory($category_id);
        $params['articles'] = $articles;
        $params['categories'] = $categories;
        $content = View::getContents(ROOT . "/modules/views/category/view.tpl", $params);
        $this->mainView->addParam("content", $content);
        $this->mainView->display();
    }
    public function editAction()
    {
        if ($_SESSION['login'] == 'admin'){
            $categories['categories'] = CategoryModel::editViewCategories();
            $content = View::getContents(ROOT . "/modules/views/category/editcategory.tpl", $categories);
            $this->mainView->addParam("content", $content);
            $this->mainView->display();
        }else{
            echo "У вас немає прав доступу";
        }
    }
}