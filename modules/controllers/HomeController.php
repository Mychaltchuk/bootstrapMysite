<?php
class HomeController extends Controller
{
    public function indexAction($parameters)
    {   
        $home['categories'] = CategoryModel::getCategories();
        $home['articles'] = ArticleModel::getArticles();
        $content = View::getContents(ROOT . "/modules/views/home/home.tpl", $home);
        $this->mainView->addParam("content", $content);
        $this->mainView->display();
    }
}
    
    
    
  
