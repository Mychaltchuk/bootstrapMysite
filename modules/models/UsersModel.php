<?php
class UsersModel 
{
    public static function register()
    {
        $new_user = new User;
        $new_user->login = $_POST['login'];
        $new_user->password = $_POST['password'];
        $new_user->username = $_POST['username'];
        $new_user->userfamily = $_POST['userfamily'];
        $new_user->userp = $_POST['userp'];
        $new_user->birthday = $_POST['birthday'];
        $new_user->email = $_POST['email'];
        $new_user->mobile = $_POST['mobile'];
        $new_user->facebook = $_POST['facebook'];
        $new_user->ip = $_SERVER["REMOTE_ADDR"];
        if(empty($new_user->login) 
                or empty($new_user->password) 
                or empty($new_user->username) 
                or empty($new_user->userfamily) 
                or empty($new_user->userp)
                ) {
                    $error['error'] = ' Заповніть всі обовязкові поля (помічені зірочкою)';
                    return $error;
        }else {
        return $new_user->registerNewUser();
        }
    }
    public static function authorization()
    {
        $auth = new User;
        $auth->login = $_POST['login'];
        $auth->password = $_POST['password'];
        return $auth->authUser();
    }
    public static function viewUsers()
    {
        $view = new User;
        return $view->userView();
    }
}

