<?php
class ArticleModel 
{
    public $article_id;
    
    /**
     * Отримання нових 5 статей
    */
    public static function getArticles()
    {
        $st = DataBase::handler()->query("SELECT * FROM articles ORDER BY date DESC LIMIT 5");
        return $st->fetchAll();
    }
    /**
     * Отримання статей відповідної категорії
    */
    public static function getArticlesByCategory($category_id) 
    {
        $st = DataBase::handler()->query("SELECT * FROM articles WHERE category_id=$category_id");
        return $st->fetchAll();
    }
    /**
    * Отримання одної статті
    */
    public static function getArticleById($article_id)
    {
        $st = DataBase::handler()->query("SELECT * FROM articles WHERE articles_id=$article_id");
        return $st->fetchAll();
    }
}
