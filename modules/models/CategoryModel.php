<?php
class CategoryModel 
{
    /**
     * Отримання всіх категорій
     * Кількість статей в відповідній категорії
    */
    public static function getCategories()
    {
        $st = DataBase::handler()->query("SELECT category.category_id, category.title, category.icon, "
                . "COUNT(articles.articles_id) AS count FROM category "
                . "LEFT JOIN articles ON articles.category_id=category.category_id "
                . "GROUP BY category.category_id, category.title ");
        return $st->fetchAll();
    }
    public static function editViewCategories()
    {
         $st = DataBase::handler()->query("SELECT * FROM category");
         return $st->fetchAll();
    }
}
