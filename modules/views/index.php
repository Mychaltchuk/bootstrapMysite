<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap</title>
        <!-- Bootstrap -->
        <link href="/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/template/css/style.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1" style="padding: 0">
                    <img src="/template/img/php.png" height=80px width=100%>
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">
                            <!-- Бренд та перемикач згруповані для кращого відображення на мобільних пристроях -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
                                <ul class="nav navbar-nav">
                                    <li><a href="http://bootstrapmysite/">БЛОГ</a></li>
                                    <li><a href="http://bootstrapmysite/about/me">ПРО МЕНЕ</a></li>
                                    <li><a href="#">КОНТАКТИ</a></li>
                                     <?php if($_SESSION['login'] == 'admin'): ?>
                                    <li><a href="http://bootstrapmysite/category/edit" class='admin'>Редагування категорій</a></li>
                                    <li><a href="http://bootstrapmysite/article/edit"class="admin">Редагування статтей</a></li>
                                    <li><a href="http://bootstrapmysite/users/view" class="admin">Users</a></li>
                                  
                                    <?php endif; ?>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <?php if($_SESSION['online']): ?>
                                    <li id='auth'><a href="http://bootstrapmysite/users/authorization">Мій кабінет</a></li>
                                    <li id='logout'><a href="http://bootstrapmysite/users/logout">Вихід</a></li>
                                    <?php else: ?>
                                    <li id='auth'><a href="http://bootstrapmysite/users/authorization">Вхід</a></li>
                                    <?php endif;?>
                                    <li><a href="http://bootstrapmysite/users/regestration">Створити обліковий запис</a></li>
                                </ul>

                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                    <div class="homebody">
                        <div class="container-fluid">
                            <div class="row">
                                <?= $content; ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                       &copy Блог Петра Михальчука
                       <a href="https://www.facebook.com/profile.php?id=100016052705538">
                           <img src="/template/img/facebook.png" width="42" height="42">
                       </a>
                    </div>
                </div> 
            </div>
        </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/template/js/bootstrap.min.js"></script>
  </body>
</html>
