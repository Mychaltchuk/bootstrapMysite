<div class="col-md-8"> 
    <?php if($_SESSION['online']):?>
        <table class="table table-hover">
            <caption style="text-align: center;"><b>Мій кабінет</b></caption>
            <tr><td><b>Логін</b></td><td><?= $_SESSION['login']?></td></tr>
            <tr><td><b>Пароль</b></td><td><?= $_SESSION['password']?></td></tr>
            <tr><td><b>Призвіще</b></td><td><?= $_SESSION['userfamily']?></td></tr>
            <tr><td><b>Імя</b></td><td><?= $_SESSION['username']?></td></tr>
        </table>
    <?php else: ?>
    <p style="text-align: center; font-size: 20px;">Авторизація</p>
    <?php if(!empty($resultstate)):?>
     <p style="color:red; text-align: center; font-size: 20px;"> <?= $resultstate; ?></p>
     <?php endif; ?>
    <form class="form-horizontal" action="http://bootstrapmysite/users/authorization" method='post'>
        <div class="form-group">
            <label class="control-label col-xs-2"><span style="color:red; font-size: 17px">*</span> Логін </label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="login" placeholder="Введіть логін">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-xs-2"> <span style="color:red; font-size: 17px">*</span> Пароль</label>
            <div class="col-xs-10">
                <input type="password" class="form-control" name="password" placeholder="Введіть пароль">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" name="authUser" class="btn btn-primary">Вхід</button>
            </div>
        </div>
    </form>
    <?php endif;?>
</div>
<div class="col-md-4">
    <div class="panel-default">
        <div class="panel-heading" style="text-align: center; border: 3px solid #DDDDDD">Список категорій</div>
            <div class="list-group">
                <?php foreach($categories as $category): ?>
                <a href="http://bootstrapmysite/category/<?= $category['category_id']?>" class="list-group-item">
                    <img src="/template/img/icons-category/<?=$category['icon']?>">
                         <span class="badge" style="margin-top: 7px"><?= $category['count'];?></span>
                    <span class="namecategory"><?= $category['title']; ?></span>
                </a>
                <?php endforeach; ?>
            </div>
    </div>
</div>     