<div class="col-md-8"> 
    <?php if(!empty($resultstate['goot'])): ?>
    <p style="color:red; text-align: center; font-size: 20px;"> <?= $resultstate['goot'] ?></p>
    <?php else:?>
    <p style="text-align: center; font-size: 20px;">Створення нового облікового запису</p>
    <?php if(!empty($resultstate['error'])): ?>
    <p style="color:red; text-align: center; font-size: 20px;"> <?= $resultstate['error'] ?></p>
    <?php endif; ?>
    <form class="form-horizontal" action="http://bootstrapmysite/users/regestration" method='post'>
        <div class="form-group">
            <label class="control-label col-xs-2"><span style="color:red; font-size: 17px">*</span> Логін </label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="login" placeholder="Введіть логін">
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="control-label col-xs-2"> <span style="color:red; font-size: 17px">*</span> Пароль</label>
            <div class="col-xs-10">
                <input type="password" class="form-control" name="password" placeholder="Введіть пароль">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2"> <span style="color:red; font-size: 17px">*</span> Ім`я</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="username" placeholder="Введіть Ім`я">
            </div>
        </div>
        <div class="form-group">
            <label  class="control-label col-xs-2"> <span style="color:red; font-size: 17px">*</span> Прізвище</label>
                <div class="col-xs-10">
            <input type="text" class="form-control" name="userfamily" placeholder="Введіть Прізвище">
                </div>
        </div>
        <div class="form-group">
            <label  class="control-label col-xs-2"> <span style="color:red; font-size: 17px">*</span> Побатькові</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="userp" placeholder="Побатькові">
            </div>
        </div>
        <div class="form-group">
            <label  class="control-label col-xs-2">Дата народження</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="birthday" placeholder="Дата народження">
            </div>
        </div>
        <div class="form-group">
            <label  class="control-label col-xs-2">Email</label>
            <div class="col-xs-10">
                <input type="inputEmail" class="form-control" name="email" placeholder="Введіть email">
            </div>
        </div>
        <div class="form-group">
            <label  class="control-label col-xs-2">Моб. тел.</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="mobile" placeholder="Мобільний телефон">
            </div>
        </div>
        <div class="form-group">
            <label  class="control-label col-xs-2"> Facebook</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="facebook"  placeholder="Facebook">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" name="addUser" class="btn btn-primary">Зареєструватися</button>
            </div>
        </div>
    </form>
<?php endif; ?>
</div>
<div class="col-md-4">
    <div class="panel-default">
        <div class="panel-heading" style="text-align: center; border: 3px solid #DDDDDD">Список категорій</div>
            <div class="list-group">
                <?php foreach($categories as $category): ?>
                <a href="http://bootstrapmysite/category/<?= $category['category_id']?>" class="list-group-item">
                    <img src="/template/img/icons-category/<?=$category['icon']?>">
                         <span class="badge" style="margin-top: 7px"><?= $category['count'];?></span>
                    <span class="namecategory"><?= $category['title']; ?></span>
                </a>
                <?php endforeach; ?>
            </div>
    </div>
</div>     