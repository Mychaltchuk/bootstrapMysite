<div class="col-md-8">
    <p><?= $goot?></p>
</div>
<div class="col-md-4" style="padding: 0">
    <div class="panel panel-default">
        <div class="panel-heading" style="text-align: center">Список категорій</div>
            <div class="list-group">
                <?php foreach($categories as $category): ?>
                <a href="http://bootstrapmysite/category/<?= $category['category_id']?>" class="list-group-item">
                    <img src="/template/img/icons-category/<?=$category['icon']?>">
                         <span class="badge" style="margin-top: 7px"><?= $category['count'];?></span>
                    <span class="namecategory"><?= $category['title'] ?></span>
                </a>
                <?php endforeach; ?>
            </div>
    </div>
</div> 