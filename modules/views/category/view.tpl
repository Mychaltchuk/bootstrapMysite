<div class="col-md-8">
    <div class="row">
    <?php foreach($articles as $key => $article): ?> 
        <div class="media">
            <a class="media-left" href="http://bootstrapmysite/article/<?=$article['articles_id']?>">
                <img src="/template/img/<?= $article['image'] ?>" width='100' height="100" alt="..." class="img-rounded">
            </a>
            <div class="media-body">
                <a href="http://bootstrapmysite/article/<?=$article['articles_id']?>">
                    <h4 class="media-heading"><?= $article['title']?></h4>
                </a>
                <p style="font-weight: bold"><?= $article['date']?></p>
                <p><?php echo mb_substr($article['mini_descriptions'], 0, 200, 'UTF-8'); ?>...</p> 
            </div>
        </div>
    <?php endforeach;?> 
    </div> 
</div> 
<div class="col-md-4">
    <div class="panel-default">
        <div class="panel-heading" style="text-align: center; border: 3px solid #DDDDDD">Список категорій</div>
            <div class="list-group">
                <?php foreach($categories as $category): ?>
                <a href="http://bootstrapmysite/category/<?= $category['category_id']?>" class="list-group-item">
                    <img src="/template/img/icons-category/<?=$category['icon']?>">
                         <span class="badge" style="margin-top: 7px"><?= $category['count'];?></span>
                    <span class="namecategory"><?= $category['title']; ?></span>
                </a>
                <?php endforeach; ?>
            </div>
    </div>
</div>     