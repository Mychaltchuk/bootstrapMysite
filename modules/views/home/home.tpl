<div class="col-md-8">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
        <!-- Обгортка для слайдів -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <a href="http://bootstrapmysite/article/<?=$articles[0]['articles_id']?>">
                <img src="/template/img/<?= $articles[0]['image'] ?>" alt="...">
                <h3><?= $articles[0]['title'] ?></h3>
                <p><?= $articles[0]['mini_descriptions'] ?></p>
                </a>
            </div>
            <div class="item">
                <a href="http://bootstrapmysite/article/<?=$articles[1]['articles_id']?>">
                <img src="/template/img/<?= $articles[1]['image'] ?>" alt="...">
                <h3><?= $articles[1]['title'] ?></h3>
                <p><?= $articles[1]['mini_descriptions'] ?></p>
                </a>
            </div> 
            <div class="item">
                <a href="http://bootstrapmysite/article/<?=$articles[2]['articles_id']?>">
                <img src="/template/img/<?= $articles[2]['image'] ?>" alt="...">
                <h3><?= $articles[2]['title'] ?></h3>
                <p><?= $articles[2]['mini_descriptions'] ?></p>
                </a>
            </div> 
            <div class="item">
                <a href="http://bootstrapmysite/article/<?=$articles[3]['articles_id']?>">
                <img src="/template/img/<?= $articles[3]['image'] ?>" alt="...">
                <h3><?= $articles[3]['title'] ?></h3>
                <p><?= $articles[3]['mini_descriptions'] ?></p>
                </a>
            </div>
            <div class="item">
                <a href="http://bootstrapmysite/article/<?=$articles[4]['articles_id']?>">
                <img src="/template/img/<?= $articles[4]['image'] ?>" alt="...">
                <h3><?= $articles[4]['title'] ?></h3>
                <p><?= $articles[4]['mini_descriptions'] ?></p>
                </a>
            </div> 
        </div>
  <!-- Управління -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Попередній</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Наступний</span>
        </a>
    </div>
    <p style="margin-top: 20px"><b>Що таке PHP?</b></p>

<p style="font-family:Geneva; font-size: 17px">PHP (англ. PHP:Hypertext Preprocessor — PHP: гіпертекстовий препроцесор), попередньо: Personal Home Page Tools — скриптова мова програмування, була створена для генерації HTML-сторінок на стороні веб-серверу. PHP є однією з найпоширеніших мов, що використовуються у сфері веб-розробок (разом із Java, .NET, Perl, Python, Ruby). PHP підтримується переважною більшістю хостинг-провайдерів. Проект за яким був створений PHP — проект з відкритими програмними кодами.

PHP інтерпретується веб-сервером в HTML-код, який передається на сторону клієнта. На відміну від таких скриптових мов програмування, як JavaScript, користувач не має доступу до PHP-коду, що є перевагою з точки зору безпеки але значно погіршує інтерактивність сторінок. Але ніщо не забороняє використовувати РНР для генерування і JavaScript-кодів які виконаються вже на стороні клієнта.</p>
</div>
<div class="col-md-4">
    <div class="panel-default">
        <div class="panel-heading" style="text-align: center; border: 3px solid #DDDDDD">Список категорій</div>
            <div class="list-group">
                <?php foreach($categories as $category): ?>
                <a href="http://bootstrapmysite/category/<?= $category['category_id']?>" class="list-group-item">
                    <img src="/template/img/icons-category/<?=$category['icon']?>">
                         <span class="badge" style="margin-top: 7px"><?= $category['count'];?></span>
                    <span class="namecategory"><?= $category['title']; ?></span>
                </a>
                <?php endforeach; ?>
            </div>
    </div>
</div>     